<?php

namespace App\Form;

use App\Entity\Car;
use App\Entity\Owner;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('numImat',TextType::class,
            [
                'label' => 'Nom',
            ])
            ->add('type',TextType::class,
            [
                'label' => 'Nom',
            ])
            // ->add('photo',FileType::class,
            // [
            //     'label' => 'Photo',
            //     'data_class' => null,
            //     'required' => false,
            //     'constraints' => [
            //         new File([
            //             'maxSize' => '10024k',
            //             'mimeTypes' => [
            //                 'image/*',
            //             ],
            //             'mimeTypesMessage' => 'Insérer une image valide',
            //         ])
            //     ]
            // ])
            ->add('inGarage',CheckboxType::class,
            [
                'label' => 'Disponible',
                'required' => false,
            ])
            ->add('owner',EntityType::class,
            [
                'expanded' => false,
                'class' => Owner::class,
                'multiple' => false,
            ])
            ->add('avalaible',CheckboxType::class,
            [
                'label' => 'Disponible',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
