<?php

namespace App\Form;

use App\Entity\Driver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DriverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
                ->add(
                    'lastName',TextType::class,
                    [
                        'label' => 'Nom',
                    ])
                ->add(
                    'firstName',TextType::class,
                    [
                        'label' => 'Prénom',
                    ])
                // ->add(
                //     'image',FileType::class,
                //     [
                //         'label' => 'Photo',
                //         'data_class' => null,
                //     ])
                ->add(
                    'address',TextType::class,
                    [
                        'label' => 'Adresse',
                    ])
                ->add(
                    'age',IntegerType::class,
                    [
                        'label' => 'Âge',
                    ])
                ->add(
                    'noTel',TelType::class,
                    [
                        'label' => 'Numéro de téléphone',
                    ])
                // ->add(
                //     'driversLicence',FileType::class,
                //     [
                //         'label' => 'Permis de conduire',
                //         'data_class' => null,
                //     ])
                ->add(
                    'avalaible',CheckboxType::class,
                    [
                        'label' => 'Disponible',
                        'required' => false,
                    ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Driver::class,
        ]);
    }
}
