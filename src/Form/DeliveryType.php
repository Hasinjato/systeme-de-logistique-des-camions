<?php

namespace App\Form;

use App\Entity\Car;
use App\Entity\Delivery;
use App\Entity\Driver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class DeliveryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateType::class,
            [
                'label' => 'Date',
                'widget' => 'single_text',
                
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('distance', IntegerType::class,
            [
                'label' => 'Distance en km',
            ])
            ->add('depart', DateTimeType::class,
            [
                'required' => false,
                'label' => 'Départ',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datetimepicker'],
            ])
            ->add('back', DateTimeType::class,
            [
                'required' => false,
                'label' => 'Rentrer',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datetimepicker'],
            ])
            ->add('driver',EntityType::class,
            [
                'label' => 'Chauffeur',
                'expanded' => false,
                'class' => Driver::class,
                'multiple' => false,
            ])
            ->add('car',EntityType::class,
            [
                'label' => 'Voiture',
                'expanded' => false,
                'class' => Car::class,
                'multiple' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Delivery::class,
        ]);
    }
}
