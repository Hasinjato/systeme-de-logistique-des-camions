<?php

namespace App\Controller;

use App\Entity\Owner;
use App\Form\OwnerType;
use App\Repository\OwnerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/owner")
 */
class OwnerController extends AbstractController
{
    /**
     * @Route("/", name="app_owner_index", methods={"GET"})
     */
    public function index(OwnerRepository $ownerRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('owner/index.html.twig', [
            'owners' => $ownerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_owner_new", methods={"GET", "POST"})
     */
    public function new(Request $request, OwnerRepository $ownerRepository, SluggerInterface $slugger): Response
    {
        $owner = new Owner();
        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $fileImg = $owner->getPhoto();
            $originalFilename = pathinfo($fileImg->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename=$slugger->slug($originalFilename);
            // $fileNameImg = md5(uniqid($safeFilename)).'.'.$fileImg->guessExtension(); 
            $fileNameImg = $safeFilename.'.'.$fileImg->guessExtension();
            $fileImg->move($this->getParameter('photos_directory_owner'), $fileNameImg); 
            $owner->setPhoto($fileNameImg);

            $ownerRepository->add($owner, true);

            return $this->redirectToRoute('app_owner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('owner/new.html.twig', [
            'owner' => $owner,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_owner_show", methods={"GET"})
     */
    public function show(Owner $owner): Response
    {
        return $this->render('owner/show.html.twig', [
            'owner' => $owner,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_owner_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Owner $owner, OwnerRepository $ownerRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(OwnerType::class, $owner);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $img = $owner->getPhoto();
            //delete img i, edit and add new
            if($img)
            {
                $fileName = $this->getParameter('photos_directory_owner'). '/' . $img;
    
                if(file_exists($fileName))
                {
                    unlink($fileName);
                }
            }
            
            $fileImg = $owner->getPhoto();
            $originalFilename = pathinfo($fileImg->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename=$slugger->slug($originalFilename);
            // $fileNameImg = md5(uniqid($safeFilename)).'.'.$fileImg->guessExtension(); 
            $fileNameImg = $safeFilename.'.'.$fileImg->guessExtension();
            $fileImg->move($this->getParameter('photos_directory_owner'), $fileNameImg); 
            $owner->setPhoto($fileNameImg);
            $ownerRepository->add($owner, true);

            return $this->redirectToRoute('app_owner_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('owner/edit.html.twig', [
            'owner' => $owner,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_owner_delete", methods={"POST"})
     */
    public function delete(Request $request, Owner $owner, OwnerRepository $ownerRepository): Response
    {
        $img = $owner->getPhoto();
        //delete img i, edit and add new
        if($img)
        {
            $fileName = $this->getParameter('photos_directory_owner'). '/' . $img;

            if(file_exists($fileName))
            {
                unlink($fileName);
            }
        }
        if ($this->isCsrfTokenValid('delete'.$owner->getId(), $request->request->get('_token'))) {
            $ownerRepository->remove($owner, true);
        }

        return $this->redirectToRoute('app_owner_index', [], Response::HTTP_SEE_OTHER);
    }
}
