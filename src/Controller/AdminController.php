<?php

namespace App\Controller;

use App\Entity\Car;
use App\Repository\CarRepository;
use App\Repository\DeliveryRepository;
use App\Repository\DriverRepository;
use App\Repository\OwnerRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="app_admin")
     */
    public function index(CarRepository $carRepository, UserRepository $userRepository, DriverRepository $driverRepository, OwnerRepository $ownerRepository, DeliveryRepository $deliveryRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        
        $type = [];
        $carCount=0;
        $countAvalaible=0;
        $countUnAvalaible=0;

        $AllCars = $carRepository->findAll();
        $carCount = count($AllCars);

        $countCarA = $carRepository->getAvalaibleCar(true);
        $countAvalaible = count($countCarA);

        $countCarB = $carRepository->getAvalaibleCar(false);
        $countUnAvalaible = count($countCarB);

        foreach ($AllCars as $car) {
            $type[] = $car->getType(); 
        }
        $allUsers = $userRepository->findAll();
        $userCount = count($allUsers);
        
        $allDrivers = $driverRepository->findAll();
        $driverCount = count($allDrivers);
        
        $allOwners = $ownerRepository->findAll();
        $ownerCount = count($allOwners);

        $deliveries1 = $deliveryRepository->findlastDeliveriesNotGo();

        $allDeliveries = $deliveryRepository->findAll();
        $deliveryCount = count($allDeliveries);

        $deliveries = $deliveryRepository->countbyDate();
        $date=[];
        $deliveryCDate=[];
        foreach($deliveries as $delivery) {
            $date[]=$delivery["dat"];
            $deliveryCDate[]=$delivery["count"];
        }
    
        return $this->render('admin/index.html.twig', [
            'carType' => json_encode($type),
            'countAvalaible' => json_encode($countAvalaible),
            'countUnAvalaible' => json_encode($countUnAvalaible),
            'carCount' => json_encode($carCount),
            'userCount' => json_encode($userCount),
            'driverCount' => json_encode($driverCount),
            'ownerCount' => json_encode($ownerCount),

            'deliveries' => $deliveries1,
            'deliveryCount' => $deliveryCount,

            'dates' => json_encode($date),
            'deliveryCountByDate' => json_encode($deliveryCDate),
        ]);
    }
}
