<?php

namespace App\Controller;

use App\Entity\Delivery;
use App\Form\DeliveryType;
use App\Repository\CarRepository;
use App\Repository\DeliveryRepository;
use App\Repository\DriverRepository;
use PHPUnit\TextUI\CliArguments\Builder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/delivery")
 */
class DeliveryController extends AbstractController
{
    /**
     * @Route("/", name="app_delivery_index", methods={"GET"})
     */
    public function index(DeliveryRepository $deliveryRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('delivery/index.html.twig', [
            'deliveries' => $deliveryRepository->findlastDeliveries(),
        ]);
    }

    /**
     * @Route("/new", name="app_delivery_new", methods={"GET", "POST"})
     */
    public function new(Request $request, DeliveryRepository $deliveryRepository, DriverRepository $driverRepository, CarRepository $carRepository): Response
    {
        $delivery = new Delivery();
        $form = $this->createForm(DeliveryType::class, $delivery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $deliveryRepository->add($delivery, true);

            return $this->redirectToRoute('app_delivery_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('delivery/new.html.twig', [
            'delivery' => $delivery,
            'form' => $form,
            'cars' => $carRepository->findAll(),
            'drivers' => $driverRepository->findAll(),
        ]);
    }

    /**
     * @Route("/{id}", name="app_delivery_show", methods={"GET"})
     */
    public function show(Delivery $delivery): Response
    {
        return $this->render('delivery/show.html.twig', [
            'delivery' => $delivery,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_delivery_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Delivery $delivery, DeliveryRepository $deliveryRepository, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(DeliveryType::class, $delivery);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $deliveryRepository->add($delivery, true);
            return $this->redirectToRoute('app_delivery_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('delivery/edit.html.twig', [
            'delivery' => $delivery,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_delivery_delete", methods={"POST"})
     */
    public function delete(Request $request, Delivery $delivery, DeliveryRepository $deliveryRepository): Response
    {
       
        if ($this->isCsrfTokenValid('delete'.$delivery->getId(), $request->request->get('_token'))) {
            $deliveryRepository->remove($delivery, true);
        }

        return $this->redirectToRoute('app_delivery_index', [], Response::HTTP_SEE_OTHER);
    }
}
