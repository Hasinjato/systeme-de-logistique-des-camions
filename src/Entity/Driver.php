<?php

namespace App\Entity;

use App\Repository\DriverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=DriverRepository::class)
 */
class Driver
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Length(max="2", maxMessage="Votre âge doit être correcte")
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min="13",
     *      max="16", 
     *      minMessage="Numéro de téléphone tros courte",
     *      maxMessage="Numéro de téléphone tros longe")
     */
    private $noTel;
    
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avalaible;

    /**
     * @ORM\OneToMany(targetEntity=Delivery::class, mappedBy="driver")
     */
    private $deliveries;

    // /**
    //  * @ORM\OneToOne(targetEntity=Delivery::class, mappedBy="driver", cascade={"persist", "remove"})
    //  */
    // private $delivery;

    public function __construct()
    {
        $this->delivery = new ArrayCollection();
        $this->deliveries = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->lastName.' '.$this->firstName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getNoTel(): ?string
    {
        return $this->noTel;
    }

    public function setNoTel(string $noTel): self
    {
        $this->noTel = $noTel;

        return $this;
    }

    public function isAvalaible(): ?bool
    {
        return $this->avalaible;
    }

    public function setAvalaible(?bool $avalaible): self
    {
        $this->avalaible = $avalaible;

        return $this;
    }

    /**
     * @return Collection<int, Delivery>
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function addDelivery(Delivery $delivery): self
    {
        if (!$this->deliveries->contains($delivery)) {
            $this->deliveries[] = $delivery;
            $delivery->setDriver($this);
        }

        return $this;
    }

    public function removeDelivery(Delivery $delivery): self
    {
        if ($this->deliveries->removeElement($delivery)) {
            // set the owning side to null (unless already changed)
            if ($delivery->getDriver() === $this) {
                $delivery->setDriver(null);
            }
        }

        return $this;
    }
}
