<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(
     *      min="8",
     *      max="9", 
     *      minMessage="Le numéro d'immatriculation est trop court",
     *      maxMessage="Le numéro d'immatriculation est tros long")
     */
    private $numImat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $inGarage;

    /**
     * @ORM\ManyToOne(targetEntity=Owner::class, inversedBy="cars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $avalaible;

    /**
     * @ORM\OneToMany(targetEntity=Delivery::class, mappedBy="car")
     */
    private $deliveries;

    // /**
    //  * @ORM\OneToOne(targetEntity=Delivery::class, mappedBy="car", cascade={"persist", "remove"})
    //  */
    // private $delivery;

    public function __construct()
    {
        $this->delivery = new ArrayCollection();
        $this->deliveries = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->type.' '.$this->numImat;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumImat(): ?string
    {
        return $this->numImat;
    }

    public function setNumImat(string $numImat): self
    {
        $this->numImat = $numImat;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function isInGarage(): ?bool
    {
        return $this->inGarage;
    }

    public function setInGarage(?bool $inGarage): self
    {
        $this->inGarage = $inGarage;

        return $this;
    }

    public function getOwner(): ?Owner
    {
        return $this->owner;
    }

    public function setOwner(?Owner $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function isAvalaible(): ?bool
    {
        return $this->avalaible;
    }

    public function setAvalaible(?bool $avalaible): self
    {
        $this->avalaible = $avalaible;

        return $this;
    }

    /**
     * @return Collection<int, Delivery>
     */
    public function getDeliveries(): Collection
    {
        return $this->deliveries;
    }

    public function addDelivery(Delivery $delivery): self
    {
        if (!$this->deliveries->contains($delivery)) {
            $this->deliveries[] = $delivery;
            $delivery->setCar($this);
        }

        return $this;
    }

    public function removeDelivery(Delivery $delivery): self
    {
        if ($this->deliveries->removeElement($delivery)) {
            // set the owning side to null (unless already changed)
            if ($delivery->getCar() === $this) {
                $delivery->setCar(null);
            }
        }

        return $this;
    }
}
